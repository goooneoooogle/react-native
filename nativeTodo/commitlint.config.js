module.exports = {
    extends: [
        "@commitlint/config-conventional"
    ],
    rules: {
        // Тело коммита должно начинаться с пустой строки
        "body-leading-blank": [2, "always"],
        // Тип всегда только в верхнем регистре
        "type-case": [2, "always", "upper-case"],
        // Тип не может быть пустым
        "type-empty": [2, "never"],
        // Типы коммитов
        "type-enum": [2, "always", ["FEAT", "ADD", "DOCS", "FIX", "SETTINGS", "REF", "STYLE", "TEST", "REMOVE"]],
    }
}